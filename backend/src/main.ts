import { AppModule } from './app.module';

import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  /**
   * Activate Class-Validator
   */
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true, // Remove additional fields from body not declare in dto
    }),
  );
  app.enableCors();

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Events Manager API')
    .setDescription('How to use Events Manager API')
    .setVersion('1.0')
    .addTag('auth')
    .addTag('events')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('api', app, document);

  await app.listen(3000);
}
bootstrap();
