import * as argon from 'argon2';
import { PrismaService } from 'src/prisma/prisma.service';

import { AuthDtoRequest, BaseAuthDtoResponse } from './models';

import { ForbiddenException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime/library';

export const jwtConstants = {
  secret: 'secret_this_should_be_longer',
};

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwtService: JwtService,
  ) {}
  async signup(dto: AuthDtoRequest): Promise<BaseAuthDtoResponse> {
    // generate the password hash
    const hash = await argon.hash(dto.password);

    // save the user in db
    try {
      const user = await this.prisma.user.create({
        data: {
          email: dto.email,
          hash,
        },
      });
      // return the user
      delete user.hash;

      const token = await this.jwtService.signAsync({
        email: user.email,
        userId: user.id,
      });

      return {
        ...user,
        token,
      };
    } catch (error) {
      if (error instanceof PrismaClientKnownRequestError) {
        if (error.code === 'P2002') {
          throw new ForbiddenException('Credential taken');
        }
      }
      throw error;
    }
  }

  async login(dto: AuthDtoRequest): Promise<BaseAuthDtoResponse> {
    // find user by email
    const user = await this.prisma.user.findUnique({
      where: {
        email: dto.email,
      },
    });
    // if user does not exist throw exception
    if (!user) {
      throw new ForbiddenException('Credentials incorrect');
    }

    // compare password

    const pwMatches = await argon.verify(user.hash, dto.password);

    if (!pwMatches) {
      throw new ForbiddenException('Credentials incorrect');
    }

    // return user

    delete user.hash;

    const token = await this.jwtService.signAsync({
      email: user.email,
      userId: user.id,
    });

    return {
      ...user,
      token,
    };
  }
}
