import { AuthService } from './auth.service';
import { AuthDtoRequest, BaseAuthDtoResponse } from './models';

import { Body, Controller, Post } from '@nestjs/common';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';

@ApiTags('auth')
@Controller('api/auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('signup')
  @ApiBody({ type: AuthDtoRequest })
  @ApiCreatedResponse({
    description: 'The created user',
    type: BaseAuthDtoResponse,
  })
  signup(@Body() dto: AuthDtoRequest) {
    return this.authService.signup(dto);
  }

  @Post('login')
  @ApiBody({ type: AuthDtoRequest })
  @ApiOkResponse({
    description: 'The logged user',
    type: BaseAuthDtoResponse,
  })
  login(@Body() dto: AuthDtoRequest) {
    return this.authService.login(dto);
  }
}
