import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

export class AuthDtoRequest {
  @ApiProperty({ required: true })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ required: true })
  @IsString()
  @IsNotEmpty()
  password: string;
}

export class BaseAuthDtoResponse {
  @ApiProperty({ required: true })
  id: string;
  @ApiProperty({ required: true })
  createdAt: Date;
  @ApiProperty({ required: true })
  updatedAt: Date;
  @ApiProperty({ required: true })
  email: string;
  @ApiProperty({ required: true })
  token: string;
  @ApiProperty({ required: true })
  firstName: string;
  @ApiProperty({ required: true })
  lastName: string;
}
