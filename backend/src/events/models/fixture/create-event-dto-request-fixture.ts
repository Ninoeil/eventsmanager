import { EventDtoRequest } from '../event.models';

export const createEventDtoRequestFixture = (
  dto?: Partial<EventDtoRequest>,
): EventDtoRequest => ({
  userId: 'userId',
  title: 'title',
  image: 'image',
  description: 'description',
  ...dto,
});
