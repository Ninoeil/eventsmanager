import { IsNotEmpty, IsString } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

export class EventDtoRequest {
  @ApiProperty({ required: true })
  @IsString()
  @IsNotEmpty()
  userId: string;

  @ApiProperty({ required: true })
  @IsString()
  @IsNotEmpty()
  title: string;

  @ApiProperty({ required: true })
  @IsString()
  @IsNotEmpty()
  image: string;

  @ApiProperty({ required: true })
  @IsString()
  @IsNotEmpty()
  description: string;
}

export class BaseEventDtoResponse {
  @ApiProperty({ required: true, minimum: 1 })
  id: number;

  @ApiProperty({ required: true })
  createdAt: Date;

  @ApiProperty({ required: true })
  updatedAt: Date;

  @ApiProperty({ required: true })
  userId: string;

  @ApiProperty({ required: true })
  title: string;

  @ApiProperty({ required: true })
  image: string;

  @ApiProperty({ required: true })
  description: string;
}
