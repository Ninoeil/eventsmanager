import { PrismaService } from '../prisma/prisma.service';
import { EventsService } from './events.service';
import { EventDtoRequest } from './models';
import { createEventDtoRequestFixture } from './models/fixture/create-event-dto-request-fixture';

jest.mock('@prisma/client', () => ({
  PrismaClient: jest.fn(() => ({
    event: {
      create: jest.fn(),
      update: jest.fn(),
      findMany: jest.fn(),
      findUnique: jest.fn(),
      delete: jest.fn(),
    },
  })),
}));

type TestConfig = {
  eventDtoRequest?: EventDtoRequest;
};

const testConfig: TestConfig = {
  eventDtoRequest: createEventDtoRequestFixture(),
};

const setup = (config: TestConfig = testConfig) => {
  const prismaService: PrismaService = new PrismaService();
  const eventsService: EventsService = new EventsService(prismaService);
  return {
    eventDtoRequest: config.eventDtoRequest,
    prismaService,
    eventsService,
  };
};

describe('EventsService', () => {
  it('should create an event', async () => {
    const { eventsService, prismaService, eventDtoRequest } = setup();

    await eventsService.createEvent(eventDtoRequest);

    expect(prismaService.event.create).toHaveBeenCalled();
  });

  it('should update an event', async () => {
    const eventId = 1;
    const { eventsService, prismaService, eventDtoRequest } = setup();
    await eventsService.updateEvent(eventId, eventDtoRequest);

    expect(prismaService.event.update).toHaveBeenCalled();
  });

  it('should get all events', async () => {
    const { eventsService, prismaService } = setup();

    await eventsService.getEvents();

    expect(prismaService.event.findMany).toHaveBeenCalled();
  });

  it('should get an event by id', async () => {
    const eventId = 1;
    const { eventsService, prismaService } = setup();

    await eventsService.getEvent(eventId);

    expect(prismaService.event.findUnique).toHaveBeenCalled();
  });

  it('should delete an event', async () => {
    const eventId = 1;
    const { eventsService, prismaService } = setup();

    await eventsService.deleteEvent(eventId);

    expect(prismaService.event.delete).toHaveBeenCalled();
  });
});
