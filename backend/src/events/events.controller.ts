import { CheckAuthGuard } from 'src/guards/check-auth.guard';

import { EventsService } from './events.service';
import { BaseEventDtoResponse, EventDtoRequest } from './models';

import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';

@ApiTags('events')
@Controller('api/events')
export class EventsController {
  constructor(private eventsService: EventsService) {}

  @UseGuards(CheckAuthGuard)
  @Post('')
  @ApiBody({ type: EventDtoRequest })
  @ApiCreatedResponse({
    description: 'The created event',
    type: BaseEventDtoResponse,
  })
  createEvent(@Body() dto: EventDtoRequest): Promise<BaseEventDtoResponse> {
    return this.eventsService.createEvent(dto);
  }

  @UseGuards(CheckAuthGuard)
  @Put(':id')
  @ApiBody({
    type: EventDtoRequest,
  })
  @ApiOkResponse({
    description: 'The updated event',
    type: BaseEventDtoResponse,
  })
  updateEvent(@Param('id') id: string, @Body() dto: EventDtoRequest) {
    return this.eventsService.updateEvent(+id, dto);
  }

  @UseGuards(CheckAuthGuard)
  @Get('')
  @ApiOkResponse({
    status: 200,
    description: 'The list of available events',
    type: [BaseEventDtoResponse],
  })
  getEvents() {
    return this.eventsService.getEvents();
  }

  @UseGuards(CheckAuthGuard)
  @Get(':id')
  @ApiOkResponse({
    status: 200,
    description: 'The retrieved event',
    type: BaseEventDtoResponse,
  })
  getEvent(@Param('id') id: string) {
    return this.eventsService.getEvent(+id);
  }

  @UseGuards(CheckAuthGuard)
  @Delete(':id')
  @ApiNoContentResponse({ description: 'No content' })
  @HttpCode(204)
  deleteEvent(@Param('id') id: string) {
    this.eventsService.deleteEvent(+id);
  }
}
