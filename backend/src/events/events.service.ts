import { PrismaService } from '../prisma/prisma.service';
import { EventDtoRequest } from './models';

import { Injectable, NotFoundException } from '@nestjs/common';

@Injectable()
export class EventsService {
  constructor(private prisma: PrismaService) {}
  async createEvent(dto: EventDtoRequest): Promise<{
    id: number;
    createdAt: Date;
    updatedAt: Date;
    userId: string;
    title: string;
    image: string;
    description: string;
  }> {
    try {
      return await this.prisma.event.create({
        data: {
          userId: dto.userId,
          title: dto.title,
          image: dto.image,
          description: dto.description,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async updateEvent(
    id: number,
    dto: EventDtoRequest,
  ): Promise<{
    id: number;
    createdAt: Date;
    updatedAt: Date;
    userId: string;
    title: string;
    image: string;
    description: string;
  }> {
    try {
      return await this.prisma.event.update({
        where: { id },
        data: {
          userId: dto.userId,
          title: dto.title,
          image: dto.image,
          description: dto.description,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async getEvents(): Promise<
    {
      id: number;
      createdAt: Date;
      updatedAt: Date;
      userId: string;
      title: string;
      image: string;
      description: string;
    }[]
  > {
    try {
      return await this.prisma.event.findMany();
    } catch (error) {
      throw error;
    }
  }

  async getEvent(eventId: number): Promise<{
    id: number;
    createdAt: Date;
    updatedAt: Date;
    userId: string;
    title: string;
    image: string;
    description: string;
  }> {
    try {
      return await this.prisma.event.findUnique({
        where: {
          id: eventId,
        },
      });
    } catch (error) {
      throw error;
    }
  }

  async deleteEvent(eventId: number) {
    try {
      const result = await this.prisma.event.delete({
        where: {
          id: eventId,
        },
      });
      return Promise.resolve(result);
    } catch (error) {
      return Promise.reject(new NotFoundException(`Event not found`));
    }
  }
}
