import { EventsController } from './events.controller';
import { EventsService } from './events.service';

import { Module } from '@nestjs/common';

@Module({
  providers: [EventsService],
  controllers: [EventsController],
})
export class EventsModule {}
