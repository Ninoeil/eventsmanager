import { PrismaService } from './prisma.service';

import { Global, Module } from '@nestjs/common';

@Global() // Make service available to all
@Module({
  providers: [PrismaService],
  exports: [PrismaService],
})
export class PrismaModule {}
