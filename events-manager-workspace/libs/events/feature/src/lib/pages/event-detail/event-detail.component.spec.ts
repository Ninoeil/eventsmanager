import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { byAltText, byRole, byText } from 'testing-library-selector';

import { EventDetailComponent } from './event-detail.component';

import { currentEventSelector } from '@events-manager-workspace/events/domain';
import { createEventDtoFixture } from '@events-manager-workspace/util-common';
import { render } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';

const ui = {
  title: byRole('heading', {
    name: /title/i,
  }),
  img: byAltText(/title/i),
  description: byText(/description/i),
  buttons: {
    edit: byRole('button', { name: /edit/i }),
    delete: byRole('button', { name: /delete/i }),
  },
  modal: {
    updateTitle: byRole('heading', { name: /update event/i }),
  },
};

const initialState = {
  events: [],
  currentEventId: null,
  loading: false,
  creatingEvent: false,
  updatingEvent: false,
  deletingEvent: false,
  error: null,
};

const setup = async () => {
  await render(EventDetailComponent, {
    providers: [
      provideMockStore({
        initialState,
        selectors: [
          { selector: currentEventSelector, value: createEventDtoFixture() },
        ],
      }),
    ],
  });

  const store = TestBed.inject(MockStore);

  return {
    store,
  };
};

describe('EventDetailComponent', () => {
  describe('when page initialize', () => {
    it('should be in initial state', async () => {
      await setup();

      expect(ui.title.get()).toBeVisible();
      expect(ui.img.get()).toBeVisible();
      expect(ui.description.get()).toBeVisible();
      expect(ui.buttons.edit.get()).toBeEnabled();
      expect(ui.buttons.delete.get()).toBeEnabled();
    });
  });

  describe('when user try to edit event', () => {
    it('should open update event dialog', async () => {
      await setup();

      const editButton = ui.buttons.edit.get();
      await userEvent.click(editButton);

      expect(ui.modal.updateTitle.get()).toBeVisible();
    });
  });

  describe('when user try to delete event', () => {
    it('should invoked delete event with correct id', async () => {
      const expectedAction = {
        eventId: 1,
        type: '[Events API] Delete Event Start',
      };
      const { store } = await setup();
      jest.spyOn(store, 'dispatch');

      const deleteButton = ui.buttons.delete.get();
      await userEvent.click(deleteButton);

      expect(store.dispatch).toHaveBeenCalledWith(expectedAction);
    });
  });
});
