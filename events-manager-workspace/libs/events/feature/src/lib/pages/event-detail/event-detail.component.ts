import { CommonModule, NgOptimizedImage } from '@angular/common';
import { Component, Input, numberAttribute, OnInit } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { Store } from '@ngrx/store';

import { EventFormComponent } from '../../event-form/event-form.component';

import {
  currentEventSelector,
  EventsApiActions,
  EventsState,
} from '@events-manager-workspace/events/domain';
import { EventsModels } from '@events-manager-workspace/util-common';

@Component({
  selector: 'app-event-detail',
  standalone: true,
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatDialogModule,
    NgOptimizedImage,
  ],
  templateUrl: './event-detail.component.html',
  styleUrl: './event-detail.component.scss',
})
export class EventDetailComponent implements OnInit {
  @Input({ alias: 'id', transform: numberAttribute }) eventId!: number;
  protected currentEvent = toSignal(this.store.select(currentEventSelector), {
    initialValue: null,
  });
  constructor(
    private dialogRef: MatDialog,
    private store: Store<EventsState>
  ) {}

  ngOnInit(): void {
    this.store.dispatch(
      EventsApiActions.getSingleEvent({ eventId: this.eventId })
    );
  }

  onDelete = (id: number): void =>
    this.store.dispatch(EventsApiActions.deleteEvent({ eventId: id }));

  onEdit = (event: EventsModels.EventDto): void => {
    const data: EventFormComponent['data'] = {
      mode: 'Edit',
      event,
    };
    this.dialogRef.open(EventFormComponent, {
      width: '600px',
      data,
    });
  };
}
