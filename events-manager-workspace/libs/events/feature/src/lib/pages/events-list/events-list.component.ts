import { CommonModule } from '@angular/common';
import { Component, inject, OnInit } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { ButtonComponent } from 'ninoeil-ui';

import { EventCardListComponent } from '../../components/event-card-list/event-card-list.component';
import { EventFormComponent } from '../../event-form/event-form.component';

import {
  EventsApiActions,
  eventsSelector,
  EventsState,
} from '@events-manager-workspace/events/domain';

@Component({
  selector: 'app-events-list',
  standalone: true,
  imports: [CommonModule, EventCardListComponent, ButtonComponent],
  templateUrl: './events-list.component.html',
  styleUrl: './events-list.component.scss',
})
export class EventsComponent implements OnInit {
  private store = inject(Store<EventsState>);
  private dialogRef = inject(MatDialog);
  events = toSignal(this.store.select(eventsSelector), { initialValue: null });

  ngOnInit(): void {
    this.store.dispatch(EventsApiActions.getEvents());
  }

  protected openEventForm = () => {
    const data: EventFormComponent['data'] = {
      mode: 'Create',
    };
    this.dialogRef.open(EventFormComponent, {
      width: '600px',
      data,
    });
  };
}
