import { inject } from '@angular/core';
import { Routes } from '@angular/router';

import { AuthService } from '@events-manager-workspace/util-auth';

export const routes: Routes = [
  {
    path: '',
    title: 'EM | Events',
    canActivate: [() => inject(AuthService).isAuthenticated()],
    loadComponent: () =>
      import('./pages/events-list/events-list.component').then(
        (mod) => mod.EventsComponent
      ),
  },
  {
    path: ':id',
    title: 'EM | Event Details',
    canActivate: [() => inject(AuthService).isAuthenticated()],
    loadComponent: () =>
      import('./pages/event-detail/event-detail.component').then(
        (mod) => mod.EventDetailComponent
      ),
  },
];
