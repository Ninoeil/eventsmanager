import { CommonModule } from '@angular/common';
import { Component, effect, inject, Inject, OnInit } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs';
import { EventDto, EventDtoRequest } from 'shared/util-common/src/lib/models';

import { InputComponent } from './input/input.component';
import { SelectComponent } from './select/select.component';

import {
  creatingEventSelector,
  Destination,
  EventForm,
  EventFormData,
  EventsApiActions,
  EventsState,
  updatingEventSelector,
} from '@events-manager-workspace/events/domain';

const DESTINATIONS: Destination[] = [
  { displayName: 'Dublin', value: 'assets/Dublin.jpg' },
  { displayName: 'Quebec', value: 'assets/Quebec.jpg' },
  { displayName: 'Tokyo', value: 'assets/Tokyo.jpg' },
] as const;

@Component({
  selector: 'app-event-form',
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule,
    MatProgressSpinner,
    InputComponent,
    SelectComponent,
  ],
  templateUrl: './event-form.component.html',
  styleUrl: './event-form.component.scss',
})
export class EventFormComponent implements OnInit {
  private store = inject(Store<EventsState>);
  private dialogRef = inject(MatDialogRef<EventFormComponent>);
  private fb = inject(FormBuilder);
  protected eventCreated = toSignal(
    this.store
      .select(creatingEventSelector)
      .pipe(filter((creatingEvent) => creatingEvent === true)),
    { initialValue: false }
  );

  closingModal = effect(() => {
    if (this.eventCreated()) {
      this.dialogRef.close();
    }
  });

  protected updatingEvent = toSignal(this.store.select(updatingEventSelector), {
    initialValue: false,
  });
  protected destinations = DESTINATIONS;
  protected eventForm = this.fb.nonNullable.group<EventForm>({
    title: this.fb.nonNullable.control(''),
    image: this.fb.nonNullable.control(''),
    description: this.fb.nonNullable.control(''),
  });

  constructor(
    @Inject(MAT_DIALOG_DATA)
    protected data: EventFormData
  ) {}
  ngOnInit(): void {
    if (this.data.event) {
      this.initForm(this.data.event);
    }
  }

  protected onSubmit = (): void => {
    const controls = this.eventForm.controls;
    const enteredEventInfo = this.getEventInfo(controls);
    if (this.data.mode === 'Create') {
      this.store.dispatch(
        EventsApiActions.createEvent({ event: enteredEventInfo })
      );
    } else {
      if (this.data.event) {
        this.store.dispatch(
          EventsApiActions.updateEvent({
            eventId: this.data.event.id,
            event: enteredEventInfo,
          })
        );
      }
    }
  };

  private initForm(event: EventDto): void {
    this.eventForm.setValue({
      title: event.title,
      image: event.image,
      description: event.description,
    });
  }

  private getEventInfo = (controls: EventForm): EventDtoRequest => ({
    userId: JSON.parse(localStorage.getItem('userData') as string).id,
    title: controls.title.value,
    image: controls.image.value,
    description: controls.description.value,
  });
}
