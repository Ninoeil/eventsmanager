import { Component, input } from '@angular/core';
import { ControlContainer, FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';

import { FirstLetterCapitalized } from '@events-manager-workspace/util-common';

@Component({
  selector: 'app-select',
  standalone: true,
  imports: [MatFormFieldModule, MatSelectModule, ReactiveFormsModule, FirstLetterCapitalized],
  templateUrl: './select.component.html',
  styleUrl: './select.component.css',
  viewProviders: [
    {
      provide: ControlContainer,
      useExisting: FormGroupDirective,
    },
  ],
})
export class SelectComponent {
  controlName = input.required<string>();
  label = input.required<string>();
  options = input.required<{ value: string; displayName: string }[]>();
}
