import { Component, input } from '@angular/core';
import { RouterLink } from '@angular/router';

import { EventListItemComponent } from '../event-list-item/event-list-item.component';

import { CardComponent } from '@events-manager-workspace/ui-common';
import { EventsModels } from '@events-manager-workspace/util-common';

@Component({
  selector: 'app-event-card-list',
  standalone: true,
  imports: [CardComponent, EventListItemComponent, RouterLink],
  templateUrl: './event-card-list.component.html',
  styleUrl: './event-card-list.component.scss',
})
export class EventCardListComponent {
  events = input<EventsModels.EventDto[] | null>(null);
}
