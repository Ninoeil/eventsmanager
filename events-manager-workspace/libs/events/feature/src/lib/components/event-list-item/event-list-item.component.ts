import { NgOptimizedImage, TitleCasePipe } from '@angular/common';
import { Component, Input } from '@angular/core';

import {
  EventsModels,
  FirstLetterCapitalized,
} from '@events-manager-workspace/util-common';

@Component({
  selector: 'app-event-list-item',
  standalone: true,
  imports: [FirstLetterCapitalized, TitleCasePipe, NgOptimizedImage],
  templateUrl: './event-list-item.component.html',
  styleUrl: './event-list-item.component.scss',
})
export class EventListItemComponent {
  @Input({ required: true }) event!: EventsModels.EventDto;
}
