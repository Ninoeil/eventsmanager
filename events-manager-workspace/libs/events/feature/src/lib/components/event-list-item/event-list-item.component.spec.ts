import { byAltText, byRole, byText } from 'testing-library-selector';

import { EventListItemComponent } from './event-list-item.component';

import { createEventDtoFixture } from '@events-manager-workspace/util-common';
import { render } from '@testing-library/angular';

const ui = {
  title: byRole('heading', { name: /title/i }),
  img: byAltText(/title/i),
  description: byText(/description/i),
};

const setup = async () => {
  const eventStub = createEventDtoFixture();
  await render(EventListItemComponent, {
    componentInputs: {
      event: eventStub,
    },
  });
};

describe('EventListItemComponent', () => {
  describe('when page initialize', () => {
    it('should display event info', async () => {
      await setup();

      const title = ui.title.get();
      const img = ui.img.get();
      const description = ui.description.get();

      expect(title).toBeVisible();
      expect(img).toBeVisible();
      expect(description).toBeVisible();
    });
  });
});
