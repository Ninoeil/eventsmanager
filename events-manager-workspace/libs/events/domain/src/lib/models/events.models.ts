import { FormControl } from '@angular/forms';

import { EventsModels } from '@events-manager-workspace/util-common';

export type EventFormData = {
  mode: 'Create' | 'Edit';
  event?: EventsModels.EventDto;
};

export type EventForm = {
  title: FormControl<string>;
  image: FormControl<string>;
  description: FormControl<string>;
};
