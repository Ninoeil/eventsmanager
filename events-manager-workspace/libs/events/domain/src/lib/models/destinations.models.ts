export type Destination = {
  displayName: 'Dublin' | 'Quebec' | 'Tokyo';
  value: 'assets/Dublin.jpg' | 'assets/Quebec.jpg' | 'assets/Tokyo.jpg';
};
