import { createReducer, on } from '@ngrx/store';

import { EventsApiActions } from './';

import { EventsModels } from '@events-manager-workspace/util-common';

export interface EventsState {
  events: EventsModels.EventDto[];
  currentEvent: EventsModels.EventDto | null;
  loading: boolean;
  creatingEvent: boolean;
  updatingEvent: boolean;
  deletingEvent: boolean;
  error: string | null;
}

export const initialState: EventsState = {
  events: [],
  currentEvent: null,
  loading: false,
  creatingEvent: false,
  updatingEvent: false,
  deletingEvent: false,
  error: null,
};

export const eventsReducer = createReducer(
  initialState,
  on(
    EventsApiActions.getEvents,
    (state): EventsState => ({
      ...state,
      loading: true,
    })
  ),
  on(
    EventsApiActions.getEventsSuccess,
    (state, action): EventsState => ({
      ...state,
      loading: false,
      events: action.events,
    })
  ),
  on(
    EventsApiActions.getEventsFailure,
    (state, action): EventsState => ({
      ...state,
      loading: false,
      error: action.errorMessage,
    })
  ),
  on(
    EventsApiActions.getSingleEvent,
    (state): EventsState => ({
      ...state,
      loading: true,
    })
  ),
  on(
    EventsApiActions.getSingleEventSuccess,
    (state, action): EventsState => ({
      ...state,
      loading: false,
      currentEvent: action.event,
    })
  ),
  on(
    EventsApiActions.getSingleEventFailure,
    (state, action): EventsState => ({
      ...state,
      loading: false,
      error: action.errorMessage,
    })
  ),
  on(
    EventsApiActions.createEvent,
    (state): EventsState => ({
      ...state,
      creatingEvent: true,
    })
  ),
  on(
    EventsApiActions.createEventSuccess,
    (state, action): EventsState => ({
      ...state,
      creatingEvent: false,
      events: [...state.events, action.event],
    })
  ),
  on(
    EventsApiActions.createEventFailure,
    (state, action): EventsState => ({
      ...state,
      creatingEvent: false,
      error: action.errorMessage,
    })
  ),
  on(
    EventsApiActions.updateEvent,
    (state): EventsState => ({
      ...state,
      updatingEvent: true,
    })
  ),
  on(
    EventsApiActions.updateEventSuccess,
    (state, action): EventsState => ({
      ...state,
      updatingEvent: false,
      events: state.events.map((event) =>
        event.id === action.event.id ? action.event : event
      ),
    })
  ),
  on(
    EventsApiActions.updateEventFailure,
    (state, action): EventsState => ({
      ...state,
      updatingEvent: false,
      error: action.errorMessage,
    })
  ),
  on(
    EventsApiActions.deleteEvent,
    (state): EventsState => ({
      ...state,
      deletingEvent: true,
    })
  ),
  on(
    EventsApiActions.deleteEventSuccess,
    (state): EventsState => ({
      ...state,
      deletingEvent: false,
      currentEvent: null,
      events: state.events.filter(
        (event) => event.id !== state.currentEvent?.id
      ),
    })
  ),
  on(
    EventsApiActions.deleteEventFailure,
    (state, action): EventsState => ({
      ...state,
      deletingEvent: false,
      error: action.errorMessage,
    })
  )
);
