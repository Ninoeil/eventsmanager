import { createFeatureSelector, createSelector } from '@ngrx/store';

import { EventsState } from './events.reducer';

const getEventsState = createFeatureSelector<EventsState>('events');

export const eventsSelector = createSelector(
  getEventsState,
  (state) => state.events
);

export const loadingSelector = createSelector(
  getEventsState,
  (state) => state.loading
);

export const errorSelector = createSelector(
  getEventsState,
  (state) => state.error
);

export const currentEventSelector = createSelector(
  getEventsState,
  (state) => state.currentEvent
);

export const creatingEventSelector = createSelector(
  getEventsState,
  (state) => state.creatingEvent
);

export const updatingEventSelector = createSelector(
  getEventsState,
  (state) => state.updatingEvent
);

export const deletingEventSelector = createSelector(
  getEventsState,
  (state) => state.deletingEvent
);
