import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { concatMap, map, mergeMap, tap } from 'rxjs';

import { EventsApiActions } from './';

import { EventsApiService } from '@events-manager-workspace/util-common';

@Injectable()
export class EventsEffects {
  getEventsStart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventsApiActions.getEvents),
      mergeMap(() =>
        this.eventsApiService.getEvents().pipe(
          map((eventsData) =>
            EventsApiActions.getEventsSuccess({
              events: eventsData,
            })
          )
        )
      )
    )
  );

  getEventStart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventsApiActions.getSingleEvent),
      mergeMap((action) =>
        this.eventsApiService.getEvent(action.eventId).pipe(
          map((event) =>
            EventsApiActions.getSingleEventSuccess({
              event: event,
            })
          )
        )
      )
    )
  );

  createEventStart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventsApiActions.createEvent),
      concatMap((action) =>
        this.eventsApiService.createEvent(action.event).pipe(
          map((event) =>
            EventsApiActions.createEventSuccess({
              event: event,
            })
          )
        )
      )
    )
  );

  updatingEventStart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventsApiActions.updateEvent),
      concatMap((action) =>
        this.eventsApiService.updateEvent(action.eventId, action.event).pipe(
          map((event) =>
            EventsApiActions.updateEventSuccess({
              event: event,
            })
          )
        )
      )
    )
  );

  updatingEventSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(EventsApiActions.updateEventSuccess),
        tap(() => {
          this.dialog.closeAll();
          this.router.navigateByUrl('events');
        })
      ),
    { dispatch: false }
  );

  deleteEventStart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EventsApiActions.deleteEvent),
      concatMap((action) =>
        this.eventsApiService
          .deleteEvent(action.eventId)
          .pipe(map(() => EventsApiActions.deleteEventSuccess()))
      )
    )
  );

  deleteEventEventSuccess = createEffect(
    () =>
      this.actions$.pipe(
        ofType(EventsApiActions.deleteEventSuccess),
        tap(() => this.router.navigateByUrl('events'))
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private eventsApiService: EventsApiService,
    private router: Router,
    private dialog: MatDialog
  ) {}
}
