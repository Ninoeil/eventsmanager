import { createAction, props } from '@ngrx/store';

import { EventsModels } from '@events-manager-workspace/util-common';

export const getEvents = createAction('[Events API] Get Events Start');

export const getEventsSuccess = createAction(
  '[Events API] Get Events Success',
  props<{ events: EventsModels.EventDto[] }>()
);

export const getEventsFailure = createAction(
  '[Events API] Get Events Fail',
  props<{ errorMessage: string }>()
);

export const getSingleEvent = createAction(
  '[Events API] Get Single Event Start',
  props<{ eventId: number }>()
);

export const getSingleEventSuccess = createAction(
  '[Events API] Get Single Event Success',
  props<{ event: EventsModels.EventDto }>()
);

export const getSingleEventFailure = createAction(
  '[Events API] Get Single Event Fail',
  props<{ errorMessage: string }>()
);

export const createEvent = createAction(
  '[Events API] Create Event Start',
  props<{ event: EventsModels.EventDtoRequest }>()
);

export const createEventSuccess = createAction(
  '[Events API] Create Event Success',
  props<{ event: EventsModels.EventDto }>()
);

export const createEventFailure = createAction(
  '[Events API] Create Event Fail',
  props<{ errorMessage: string }>()
);

export const updateEvent = createAction(
  '[Events API] Update Event Start',
  props<{ eventId: number; event: EventsModels.EventDtoRequest }>()
);

export const updateEventSuccess = createAction(
  '[Events API] Update Event Success',
  props<{ event: EventsModels.EventDto }>()
);

export const updateEventFailure = createAction(
  '[Events API] Update Event Fail',
  props<{ errorMessage: string }>()
);

export const deleteEvent = createAction(
  '[Events API] Delete Event Start',
  props<{ eventId: number }>()
);
export const deleteEventSuccess = createAction(
  '[Events API] Delete Event Success'
);
export const deleteEventFailure = createAction(
  '[Events API] Delete Event Fail',
  props<{ errorMessage: string }>()
);
