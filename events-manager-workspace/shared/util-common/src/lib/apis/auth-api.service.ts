import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';

import { UserDto, UserDtoRequest } from '../models';

import { environment } from '@events-manager-workspace/util-environments';

const AUTH = 'auth';

@Injectable({
  providedIn: 'root',
})
export class AuthApiService {
  private http = inject(HttpClient);
  signup = (request: UserDtoRequest) => {
    return this.http.post<UserDto>(
      environment.server + AUTH + '/signup',
      request
    );
  };

  login = (request: UserDtoRequest) => {
    return this.http.post<UserDto>(
      environment.server + AUTH + '/login',
      request
    );
  };
}
