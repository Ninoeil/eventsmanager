import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';

import { EventDto, EventDtoRequest } from '../models';

import { environment } from '@events-manager-workspace/util-environments';

const EVENTS = 'events';

@Injectable({
  providedIn: 'root',
})
export class EventsApiService {
  private http = inject(HttpClient);
  getEvents = () => {
    return this.http.get<EventDto[]>(environment.server + EVENTS);
  };

  getEvent = (eventId: number) => {
    return this.http.get<EventDto>(environment.server + EVENTS + '/' + eventId);
  };

  createEvent = (request: EventDtoRequest) => {
    return this.http.post<EventDto>(environment.server + EVENTS, request);
  };

  updateEvent = (eventId: number, request: EventDtoRequest) => {
    return this.http.put<EventDto>(
      environment.server + EVENTS + `/${eventId}`,
      request
    );
  };

  deleteEvent = (eventId: number) => {
    return this.http.delete(environment.server + EVENTS + '/' + eventId);
  };
}
