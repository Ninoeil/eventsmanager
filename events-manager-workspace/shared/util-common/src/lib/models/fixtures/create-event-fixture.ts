import { EventDto } from '../events-api.models';

export const createEventDtoFixture = (event?: Partial<EventDto>): EventDto => ({
  id: 1,
  createdAt: new Date().toString(),
  updatedAt: new Date().toString(),
  title: 'title',
  image: 'image',
  description: 'description',
  ...event,
});
