export type UserDto = {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  email: string;
  token: string;
  firstName?: string;
  lastName?: string;
};

export type UserDtoRequest = {
  email: string;
  password: string;
};
