export type EventDto = {
  id: number;
  createdAt: string;
  updatedAt: string | null;
  title: string;
  image: string;
  description: string;
};

// Requests DTO

export type EventDtoRequest = {
  userId: string;
  title: string;
  image: string;
  description: string;
};
