import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firstLetterCapitalized',
  standalone: true,
})
export class FirstLetterCapitalized implements PipeTransform {
  transform(value: string) {
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
}
