import { environment as EnvironmentProd } from './lib/util-environments/environment.prod';

export * from './lib/util-environments/environment';
export { EnvironmentProd };
