import { HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { switchMap, take } from 'rxjs';

import { AuthState } from '../state';
import { userSelector } from '../state/auth.selectors';

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  const store = inject(Store<AuthState>);
  return store.select(userSelector).pipe(
    take(1),
    switchMap((user) => {
      const authReq = user
        ? req.clone({
            headers: req.headers.set('Authorization', 'Bearer ' + user.token),
          })
        : req;
      return next(authReq);
    })
  );
};
