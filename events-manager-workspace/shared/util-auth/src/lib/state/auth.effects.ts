import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, of } from 'rxjs';

import { AuthService } from '../services/auth.service';
import { AuthApiActions } from './actions/auth.actions';

import { AuthModels } from '@events-manager-workspace/util-common';

@Injectable()
export class AuthEffects {
  loginStart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthApiActions.loginStart),
      exhaustMap((action) => {
        const request = action;
        return this.authService.login(request).pipe(
          map((user) =>
            AuthApiActions.authenticateSuccess({
              user: this.handleAuthenticationSuccess(user),
            })
          ),
          catchError((err: HttpErrorResponse) =>
            this.handleAuthenticationError(err)
          )
        );
      })
    )
  );

  signupStart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthApiActions.signupStart),
      exhaustMap((action) => {
        const request = action;
        return this.authService.signup(request).pipe(
          map((user) =>
            AuthApiActions.authenticateSuccess({
              user: this.handleAuthenticationSuccess(user),
            })
          ),
          catchError((err: HttpErrorResponse) =>
            this.handleAuthenticationError(err)
          )
        );
      })
    )
  );

  private handleAuthenticationSuccess = (user: AuthModels.UserDto) => {
    localStorage.setItem('userData', JSON.stringify(user));
    return user;
  };

  private handleAuthenticationError = (err: HttpErrorResponse) => {
    let errorMessage = 'An unknown error occurred';
    if (err.error.message === 'Credential taken') {
      errorMessage = 'Already existing user';
    }
    return of(AuthApiActions.authenticateFail({ errorMessage }));
  };

  constructor(private actions$: Actions, private authService: AuthService) {}
}
