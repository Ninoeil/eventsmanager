import { createActionGroup, props } from '@ngrx/store';

import { AuthModels } from '@events-manager-workspace/util-common';

export const AuthApiActions = createActionGroup({
  source: 'Auth API',
  events: {
    'Login Start': props<{ email: string; password: string }>(),
    'Signup Start': props<{ email: string; password: string }>(),
    'Authenticate Success': props<{ user: AuthModels.UserDto }>(),
    'Authenticate Fail': props<{ errorMessage: string }>(),
  },
});
