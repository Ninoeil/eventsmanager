import { createFeatureSelector, createSelector } from '@ngrx/store';

import { AuthState } from './auth.reducer';

export const selectAuthState = createFeatureSelector<AuthState>('auth');
export const userSelector = createSelector(
  selectAuthState,
  (state) => state.user
);

export const authErrorSelector = createSelector(
  selectAuthState,
  (state) => state.authError
);

export const isAuthenticatedSelector = createSelector(
  selectAuthState,
  (state) => state.isAuthenticated
);
