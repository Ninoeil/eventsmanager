import { createReducer, on } from '@ngrx/store';

import { AuthActions } from './actions/auth-api.actions';
import { AuthApiActions } from './actions/auth.actions';

import { AuthModels } from '@events-manager-workspace/util-common';

export interface AuthState {
  loading: boolean;
  user: AuthModels.UserDto | null;
  authError: string | null;
  isAuthenticated: boolean;
}

const initialState: AuthState = {
  loading: false,
  user: null,
  authError: null,
  isAuthenticated: false,
};

export const authReducer = createReducer(
  initialState,
  on(AuthApiActions.loginStart, (state) => ({ ...state, loading: true })),
  on(AuthApiActions.signupStart, (state) => ({ ...state, loading: true })),
  on(AuthApiActions.authenticateSuccess, (state, { user }) => ({
    ...state,
    loading: false,
    user: user,
    isAuthenticated: true,
  })),
  on(AuthApiActions.authenticateFail, (state, { errorMessage }) => ({
    ...state,
    loading: false,
    authError: errorMessage,
  })),
  on(AuthActions.logout, (state) => {
    localStorage.removeItem('userData');
    return { ...state, user: null, isAuthenticated: false };
  }),
  on(AuthActions.autoLogin, (state, { user }) => ({
    ...state,
    user,
    isAuthenticated: user ? true : false,
  }))
);
