import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AnchorButtonComponent } from 'ninoeil-ui';

@Component({
  selector: 'app-auth-modal-footer',
  standalone: true,
  imports: [AnchorButtonComponent],
  templateUrl: './auth-modal-footer.component.html',
  styleUrl: './auth-modal-footer.component.scss',
})
export class AuthModalFooterComponent {
  @Input({ required: true }) buttonLabel!: string;
  @Input({ required: true }) paragraphLabel!: string;
  @Output() clicked = new EventEmitter();
}
