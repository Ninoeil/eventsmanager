import { AsyncPipe } from '@angular/common';
import { Component, signal, WritableSignal } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { tap } from 'rxjs';

import { AUTH_MODE, AuthType } from '../../models';
import { AuthApiActions, AuthState, AuthStateSelectors } from '../../state';
import { AuthFormComponent } from '../auth-form/auth-form.component';
import { AuthModalFooterComponent } from '../auth-modal-footer/auth-modal-footer.component';

import {
  AuthModels,
  NotificationsService,
} from '@events-manager-workspace/util-common';

@Component({
  standalone: true,
  imports: [
    MatCardModule,
    MatButtonModule,
    ReactiveFormsModule,
    AsyncPipe,
    AuthFormComponent,
    AuthModalFooterComponent,
  ],
  templateUrl: './auth-modal.component.html',
  styleUrl: './auth-modal.component.scss',
})
export class AuthModalComponent {
  protected mode: WritableSignal<AuthType> = signal(AUTH_MODE.login);

  constructor(
    private dialogRef: MatDialogRef<AuthModalComponent>,
    private store: Store<AuthState>,
    private notificationsService: NotificationsService
  ) {
    this.store
      .select(AuthStateSelectors.userSelector)
      .pipe(
        takeUntilDestroyed(),
        tap((loggedUser) => {
          if (loggedUser) {
            this.dialogRef.close();
          }
        })
      )
      .subscribe();

    this.store
      .select(AuthStateSelectors.authErrorSelector)
      .pipe(
        takeUntilDestroyed(),
        tap((authError) => {
          if (authError) {
            this.notificationsService.openSnackBar(authError);
          }
        })
      )
      .subscribe();
  }

  protected onFormSubmitted(userCredentials: AuthModels.UserDtoRequest) {
    if (this.mode() === AUTH_MODE.login) {
      this.store.dispatch(AuthApiActions.loginStart(userCredentials));
    } else {
      this.store.dispatch(AuthApiActions.signupStart(userCredentials));
    }
  }

  protected modeChanged() {
    if (this.mode() === AUTH_MODE.login) {
      this.mode.set(AUTH_MODE.signup);
    } else {
      this.mode.set(AUTH_MODE.login);
    }
  }
}
