import { TitleCasePipe } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';

import { AuthForm, AuthType } from '../../models';

import {
  EmailInputComponent,
  PasswordInputComponent,
} from '@events-manager-workspace/ui-common';
import { AuthModels } from '@events-manager-workspace/util-common';

@Component({
  selector: 'app-auth-form',
  standalone: true,
  imports: [
    EmailInputComponent,
    PasswordInputComponent,
    ReactiveFormsModule,
    MatDialogModule,
    TitleCasePipe,
    MatButtonModule,
  ],
  templateUrl: './auth-form.component.html',
  styleUrl: './auth-form.component.scss',
})
export class AuthFormComponent {
  @Input({ required: true }) mode!: AuthType;
  @Output() formSubmitted = new EventEmitter<AuthModels.UserDtoRequest>();
  protected authForm: FormGroup<AuthForm> = this.fb.nonNullable.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]],
  });

  constructor(private fb: FormBuilder) {}

  protected onSubmit() {
    if (!this.authForm.valid) {
      this.authForm.markAllAsTouched();
      return;
    }

    this.formSubmitted.emit({
      email: this.authForm.controls.email.value,
      password: this.authForm.controls.password.value,
    });

    this.authForm.reset();
  }
}
