import { inject, Injectable } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { Store } from '@ngrx/store';

import { AuthStateSelectors } from '../state';

import {
  AuthApiService,
  AuthModels,
} from '@events-manager-workspace/util-common';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private authApi = inject(AuthApiService);
  private store = inject(Store);
  isAuthenticated = toSignal(
    this.store.select(AuthStateSelectors.isAuthenticatedSelector),
    { initialValue: false }
  );

  login(request: AuthModels.UserDtoRequest) {
    return this.authApi.login(request);
  }

  signup(request: AuthModels.UserDtoRequest) {
    return this.authApi.signup(request);
  }
}
