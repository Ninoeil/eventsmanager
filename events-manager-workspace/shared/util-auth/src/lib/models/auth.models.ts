import { FormControl } from '@angular/forms';

export type AuthType = 'signup' | 'login';
export type AuthMode = {
  signup: AuthType;
  login: AuthType;
};

export type AuthForm = {
  email: FormControl<string>;
  password: FormControl<string>;
};
