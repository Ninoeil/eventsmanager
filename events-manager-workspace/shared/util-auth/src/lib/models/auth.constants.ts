import { AuthMode } from './auth.models';

export const AUTH_MODE = {
  login: 'login',
  signup: 'signup',
} as const satisfies AuthMode;
