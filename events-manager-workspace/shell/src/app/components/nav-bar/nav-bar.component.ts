import { AsyncPipe } from '@angular/common';
import { Component } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Router, RouterLink } from '@angular/router';
import { Store } from '@ngrx/store';

import {
  AuthModalComponent,
  AuthStore,
} from '@events-manager-workspace/util-auth';

@Component({
  selector: 'app-nav-bar',
  standalone: true,
  imports: [MatToolbarModule, MatButtonModule, RouterLink, AsyncPipe],
  templateUrl: './nav-bar.component.html',
  styleUrl: './nav-bar.component.scss',
})
export class NavBarComponent {
  user = toSignal(
    this.store.select(AuthStore.AuthStateSelectors.userSelector),
    { initialValue: null }
  );
  constructor(
    private dialog: MatDialog,
    private store: Store,
    private router: Router
  ) {}
  openAuthModal = () => {
    this.dialog.open(AuthModalComponent, {
      width: '600px',
    });
  };
  logout = () => {
    this.store.dispatch(AuthStore.AuthActions.logout());
    this.router.navigate(['']); // TODO: Should be centralized
  };
}
