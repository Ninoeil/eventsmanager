import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-authorized-layout',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './authorized-layout.component.html',
  styleUrl: './authorized-layout.component.scss',
})
export class AuthorizedLayoutComponent {}
