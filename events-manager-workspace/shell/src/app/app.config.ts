import {
  provideHttpClient,
  withFetch,
  withInterceptors,
} from '@angular/common/http';
import { ApplicationConfig } from '@angular/core';
import { provideClientHydration } from '@angular/platform-browser';
import { provideAnimations } from '@angular/platform-browser/animations';
import {
  PreloadAllModules,
  provideRouter,
  withComponentInputBinding,
  withPreloading,
} from '@angular/router';
import { provideEffects } from '@ngrx/effects';
import { provideStore } from '@ngrx/store';

import { routes } from './app.routes';

import {
  EventsEffects,
  eventsReducer,
} from '@events-manager-workspace/events/domain';
import {
  AuthEffects,
  authInterceptor,
  AuthReducer,
} from '@events-manager-workspace/util-auth';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(
      routes,
      withComponentInputBinding(),
      withPreloading(PreloadAllModules)
    ),
    provideClientHydration(),
    provideHttpClient(withFetch(), withInterceptors([authInterceptor])),
    provideAnimations(),
    provideStore({
      auth: AuthReducer.authReducer,
      events: eventsReducer,
    }),
    provideEffects(AuthEffects.AuthEffects, EventsEffects),
  ],
};
