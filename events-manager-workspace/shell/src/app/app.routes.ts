import { inject } from '@angular/core';
import { Routes } from '@angular/router';

import { HomeComponent } from './pages/home.component';

import { AuthService } from '@events-manager-workspace/util-auth';

export const routes: Routes = [
  { path: '', component: HomeComponent, title: 'EM | Home' },
  {
    path: 'events',
    canActivate: [() => inject(AuthService).isAuthenticated()],
    loadChildren: () =>
      import('@events-manager-workspace/events/feature').then(
        (mod) => mod.routes
      ),
  },
];
