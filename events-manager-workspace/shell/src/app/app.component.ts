import { CommonModule } from '@angular/common';
import { afterNextRender, Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { Store } from '@ngrx/store';

import { NavBarComponent } from './components';
import { AuthorizedLayoutComponent } from './layouts';

import { AuthStore } from '@events-manager-workspace/util-auth';
import { AuthModels } from '@events-manager-workspace/util-common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    NavBarComponent,
    AuthorizedLayoutComponent,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  constructor(private store: Store) {
    afterNextRender(() => {
      const userData = localStorage.getItem('userData');
      const user: AuthModels.UserDto | null = userData
        ? JSON.parse(userData)
        : null;
      this.store.dispatch(AuthStore.AuthActions.autoLogin({ user }));
    });
  }
}
